import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Quiz from './Tugas/Quiz3'
import Youtube from './Tugas/Tugas12/App'

export default function App() {
  return (
    //<Quiz/>
    <Youtube/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
