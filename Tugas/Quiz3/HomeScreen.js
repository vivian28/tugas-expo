import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    
    // Kode di sini
    this.setState({totalPrice: this.state.totalPrice + parseInt(price)})
    //STEP 5, buat fungsinya! jadi itu dia ngubah state, makanya pake setState, yang diubah itu totalPrice, jadi totalPrice ditambah sama price (yang didalam kurung updatePrice), terus karena price masih dalam bentuk string, kita mesti ubah ke Number, makanya pake parseInt, kalau mau pake fungsi 'Number' juga bisa
  }


  render() {
    console.log(data)
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
            {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              <Text style={styles.headerText}>{this.props.route.params.userName}</Text>
            </Text>
            {/* jadi gitu buat nge pass paramsnya */}
            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
              {/* sesuai soal */}
              {/* jadi ini STEP 1, bikin statenya*/}
              {/* STEP 6, bungkus pake function currencyFormat biar dalam bentuk Rp */}
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)

        // Lanjutkan di bawah ini!
        */}
        {/*<FlatList
          data={data.produk} //import dari data.json, produk soalnya di file .json nya itu yang pertama "produk"
          keyExtractor={(item, index) => index.toString()}  //return indexnya aja (yang pake arrow), terus ubah ke string
          renderItem={({ item }) => {
            return <ListItem data={item}/>  //data itu yang data dibawah render itu, kalo item itu emang ya!
          }}
          numColumns={2} //ada ya fungsi ini! buat nambah jumlah collumngnya
        />*/}
        {/* jadi ini jawaban untuk nomer 2, dibawah yg udah tambahan soal bonus */}
        <FlatList
          data={data.produk}
          keyExtractor={(item, index) => index.toString()}
          renderItem = {({item}) => {
            return (
              // PILIHAN PERTAMA, pake TOUCHABLE OPACITY
              //<TouchableOpacity onPress={() => this.updatePrice(item.harga)}>
                //ini STEP 2, kita tambahin touchableOpacity, trs updatePrice(fungsi yang ada diatas) trs dalemnya isi harganya(yang mau di update)*/}
                //<ListItem updatePrice={this.updatePrice} data={item} />
                  //STEP 3, tambahin fungsi updatePrice*/}
              //</TouchableOpacity>
              // PILIHAN KEDUA, di PASSING ke THIS.ITEMS
              <ListItem updatePrice={this.updatePrice.bind(this)} data={item} />
              //Beda PILIHAN PERTAMA sama KEDUA? kalo PERTAMA itu dia jadinya satu kotak bisa dipencet buat nambah harga, kalo yang KEDUA, cuman tombol "BELI" nya aja yang bisa dipencet, jadi yang lebih bagus yang KEDUA ;)
            )
          }}
          numColumns={2} //ada ya fungsi ini! buat nambah jumlah collumngnya
        />
      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const {data, updatePrice} = this.props //STEP 4, ganti data yang sebelumnya
    //const data = this.props.data
    return (
      //PILIHAN KETIGA mirip sama PILIHAN PERTAMA. Caranya view yang dibawah ini diganti jadi touchableOpacity, terus dalemnya kasi fungsi yang onPress, udah deh :D
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain'/>
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue' 
          onPress={() => updatePrice(data.harga)}
        />
        {/* onPress dipake di PILIHAN KEDUA doang */}
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    padding: 4,
    margin: 8,
    backgroundColor: 'white',
    alignItems: 'center'
    //tambahin biar ga terlalu dempet, detail ampe backgroundnya!
  },
  itemImage: {
    height: 50,
    width: 100
  },
  itemName: {
    fontWeight: 'bold',
    minHeight: 40
    //jeda kebawah
  },
  itemPrice: {
    fontSize: 18,
    color: 'blue'
  },
  itemStock: {
    //ga perlu
  },
  itemButton: {
    //yang ga ada bisa di delete aja
  },
  buttonText: {
    //ini juga sebenernya ga perlu, jadi bisa dhapus
  }
})
