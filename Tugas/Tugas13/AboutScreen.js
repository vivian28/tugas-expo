import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/MaterialIcons'

import { Colors } from 'react-native/Libraries/NewAppScreen';

export default class About extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.icons}>
                    <TouchableOpacity>
                        <Icon2 name="arrow-back" size={40}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.judul}>
                    <Text style={styles.textTitle}>About Developer</Text>
                </View>
                <View style={styles.pp}>
                    <Image source={require('./photo/fixed.png')} style={{width: 150, height: 150}}/>
                </View>
                <View>
                    <Text style={styles.text2}>Name: Kenzie Starlet</Text>
                </View>
                <View>
                    <Text style={styles.text3}>Social Media:</Text>
                </View>
                <View style={styles.box}>
                    <View  style={styles.logo}>
                        <Icon name="logo-facebook" size={30}/>
                        <Icon name="logo-twitter" size={30}/>
                        <Icon name="logo-instagram" size={30}/>
                    </View>
                    <View>
                        <Text style={styles.sosmed}> Kenzie Starlet</Text>
                        <Text style={styles.sosmed}> @kenzstar</Text>
                        <Text style={styles.sosmed}> @its_kenzie</Text>
                    </View>
                </View>
                <View>
                    <Text style={styles.text3}>Portofolio:</Text>
                </View>
                <View style={styles.box2}>
                    <View style={styles.logo}>
                        <Icon name="logo-github" size={30}/>
                    </View>
                    <View>
                        <Text style={styles.sosmed}> @kenzstar6</Text>
                    </View>
                </View>
                <View>
                    <TouchableOpacity>
                        <View style={styles.box3}>
                            <Text style={styles.next}>Next</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ('#79D9F8')
    },
    icons: {
        marginTop: 30,
        marginLeft: 25
    },
    judul: {
        marginTop: 10,
        alignItems: 'center'
    },
    textTitle: {
        fontFamily: 'sans-serif-medium',
        fontSize: 35
    },
    pp: {
        marginTop: 40,
        alignItems: 'center'
    },
    text2: {
        marginTop: 30,
        marginLeft: 50,
        fontSize: 30,
        alignSelf: 'flex-start'
    },
    text3: {
        marginTop: 20,
        marginLeft: 50,
        fontSize: 30,
        alignSelf: 'flex-start'
    },
    box: {
        backgroundColor: '#E5E5E5',
        height: 93,
        width: 310,
        justifyContent: 'flex-start',
        borderRadius: 8,
        alignSelf: 'center',
        flexDirection: 'row'
    },
    sosmed: {
        fontSize: 20
    },
    logo: {
        marginLeft: 15
    },
    box2: {
        backgroundColor: '#E5E5E5',
        height: 32,
        width: 310,
        justifyContent: 'flex-start',
        borderRadius: 8,
        alignSelf: 'center',
        flexDirection: 'row'
    },
    box3: {
        backgroundColor: '#0C1B6C',
        marginTop: 45,
        height: 45,
        width: 100,
        borderRadius: 8,
        alignSelf: 'center'
    },
    next: {
        fontSize: 30,
        color: 'white',
        alignSelf: 'center'
    }
})