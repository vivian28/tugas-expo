import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

export default class Register extends React.Component {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.icons}>
                    <TouchableOpacity>
                        <Icon2 name="arrow-back" size={40}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.title}>
                    <Image source={require('./logo/logo.png')} style={{width: 400, height: 100}}/>
                </View>
                <View>
                    <Text style={styles.register2}>Register</Text>
                    <Text style={styles.text}>Username/Email</Text>
                    <TouchableOpacity>
                        <View style={styles.box}/>
                    </TouchableOpacity>
                    <Text style={styles.text}>Password</Text>
                    <TouchableOpacity>
                        <View style={styles.box}/>
                    </TouchableOpacity>
                    <Text style={styles.text}>Confirm Password</Text>
                    <TouchableOpacity>
                        <View style={styles.box}/>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity>
                        <View style={styles.button}>
                            <Text style={styles.text3}>Register</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#79D9F8"
    },
    icons: {
        marginTop: 30,
        marginLeft: 25
    },
    title: {
        height: 160,
        flexDirection: 'row',
        alignItems: 'center'
    },
    register2: {
        fontFamily: 'sans-serif-medium',
        fontSize: 50,
        textAlign: 'center'
    },
    text: {
        paddingTop: 35,
        fontSize: 16,
        marginLeft: 60
    },
    box: {
        backgroundColor: 'white',
        height: 35,
        width: 300,
        justifyContent: 'center',
        borderRadius: 8,
        alignSelf: 'center'
    },
    button: {
        marginTop: 30,
        alignSelf: 'center'
    },
    text2: {
        fontSize: 30,
        color: 'white',
        backgroundColor: '#0C1B6C',
        borderRadius: 8
    },
    button: {
        backgroundColor: '#0C1B6C',
        marginTop: 60,
        height: 45,
        width: 150,
        borderRadius: 8,
        alignSelf: 'center'
    },
    text3: {
        fontSize: 30,
        color: 'white',
        alignSelf: 'center'
    }
})