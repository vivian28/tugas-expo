import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Skill extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.icon}>
                    <TouchableOpacity>
                        <Icon name="arrow-back" size={40}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.vector}>
                    <Text style={styles.title}>Teknologi</Text>
                </View>
                <View>
                    <Image source={require('./photo/gitlab.png')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Advance</Text>
                        <Text style={styles.font}>90%</Text>
                    </View>
                    <Image source={require('./photo/github.jpeg')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Intermediate</Text>
                        <Text style={styles.font}>70%</Text>
                    </View>
                    <Image source={require('./photo/android.png')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Intermediate</Text>
                        <Text style={styles.font}>60%</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ('#5FFF9F')
    },
    icon: {
        marginTop: 30,
        marginLeft: 25
    },
    title: {
        fontSize: 21,
        fontFamily: 'sans-serif-medium'
    },
    vector: {
        top: -20,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: 150,
        height: 50,
        borderRadius: 100,
        backgroundColor: 'white'
    },
    foto: {
        height: 120,
        width: 350,
        marginTop: 20,
        borderRadius: 30,
        alignSelf: 'center'
    },
    font: {
        fontSize: 25
    },
    textContent: {
        marginLeft: 35,
        marginRight: 35,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})