import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Colors } from 'react-native/Libraries/NewAppScreen';

export default class Login extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        return(
            <View style={styles.container}>
                <View style={styles.title}>
                    <Image source={require('./logo/logo.png')} style={{width: 400, height: 100}}/>
                </View>
                <View>
                    <Text style={styles.login}>Login</Text>
                    <Text style={styles.text}>Username/Email</Text>
                    <TouchableOpacity>
                        <View style={styles.box}/>
                    </TouchableOpacity>
                    <Text style={styles.text}>Password</Text>
                    <TouchableOpacity>
                        <View style={styles.box}/>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigate("DrawerScreen")}>
                        <View style={styles.button}>
                            <Text style={styles.text2}>Login</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.bottom}>
                    <Text style={styles.text3}>Don't have an account?</Text>
                    <TouchableOpacity onPress={() => navigate("Register")}>
                        <Text style={styles.register}>Register here</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#79D9F8"
    },
    title: {
        height: 300,
        flexDirection: 'row',
        alignItems: 'center'
    },
    login: {
        marginTop: -20,
        fontFamily: 'sans-serif-medium',
        fontSize: 50,
        textAlign: 'center'
    },
    text: {
        paddingTop: 35,
        fontSize: 16,
        marginLeft: 60
    },
    box: {
        backgroundColor: 'white',
        height: 35,
        width: 300,
        justifyContent: 'center',
        borderRadius: 8,
        alignSelf: 'center'
    },
    button: {
        backgroundColor: '#0C1B6C',
        marginTop: 45,
        height: 45,
        width: 110,
        borderRadius: 8,
        alignSelf: 'center'
    },
    text2: {
        fontSize: 30,
        color: 'white',
        alignSelf: 'center'
    },
    bottom: {
        marginTop: 40,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    text3: {
        fontSize: 15
    },
    register: {
        fontSize: 15,
        color: 'red'
    }
})