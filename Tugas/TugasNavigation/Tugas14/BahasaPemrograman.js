import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Skill extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.icon}>
                    <TouchableOpacity>
                        <Icon name="arrow-back" size={40}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.vector}>
                    <Text style={styles.title}>Bahasa Pemrograman</Text>
                </View>
                <ScrollView>
                    <Image source={require('./photo/javascript.png')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Advance</Text>
                        <Text style={styles.font}>90%</Text>
                    </View>
                    <Image source={require('./photo/pythontm.png')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Intermediate</Text>
                        <Text style={styles.font}>60%</Text>
                    </View>
                    <Image source={require('./photo/java.png')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Basic</Text>
                        <Text style={styles.font}>30%</Text>
                    </View>
                    <Image source={require('./photo/Cplus.jpg')} style={styles.foto}/>
                    <View style={styles.textContent}>
                        <Text style={styles.font}>Advance</Text>
                        <Text style={styles.font}>70%</Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ('#FF8080')
    },
    icon: {
        marginTop: 30,
        marginLeft: 25
    },
    title: {
        fontSize: 21,
        fontFamily: 'sans-serif-medium'
    },
    vector: {
        top: -20,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: 250,
        height: 50,
        borderRadius: 100,
        backgroundColor: 'white'
    },
    foto: {
        height: 120,
        width: 350,
        marginTop: 20,
        borderRadius: 30,
        alignSelf: 'center'
    },
    font: {
        fontSize: 25
    },
    textContent: {
        marginLeft: 35,
        marginRight: 35,
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
})