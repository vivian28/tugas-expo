import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ImageBackground, ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Skill extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.icon}>
                    <TouchableOpacity>
                        <Icon name="arrow-back" size={40}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.vector}>
                    <Text style={styles.home}>HOME</Text>
                </View>
                <View>
                <View style={styles.pp}>
                    <Image source={require('./photo/people.png')} style={{width: 110, height: 110}}/>
                    <View style={styles.acc}>
                        <Text style={{fontSize: 30, fontFamily: 'serif'}}>Hi,</Text>
                        <Text style={{fontSize: 30, fontFamily: 'serif'}}>Rayn_41</Text>
                    </View>
                </View>
                    <Text style={styles.text2}>Daftar Skill</Text>
                </View>
                <ScrollView>
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => navigate("Bahasa")}>
                            <View>
                                <ImageBackground source={require('./photo/bahasapemrograman.jpeg')} style={styles.foto}>
                                    <Text style={styles.text3}>Bahasa Pemrograman</Text>
                                </ImageBackground>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate("Framework")}>
                            <View>
                                <ImageBackground source={require('./photo/framework.jpg')} style={styles.foto}>
                                    <Text style={styles.text3}>Framework/Library</Text>
                                </ImageBackground>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate("Teknologi")}>
                            <View>
                                <ImageBackground source={require('./photo/techyes2.png')} style={styles.foto}>
                                    <Text style={styles.text3}>Teknologi</Text>
                                </ImageBackground>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: ('#79D9F8')
    },
    icon: {
        marginTop: 30,
        marginLeft: 25
    },
    home: {
        marginTop: 28,
        fontSize: 24,
        height: 50,
        fontFamily: 'sans-serif-medium'
    },
    vector: {
        top: -20,
        alignItems: 'center',
        alignSelf: 'center',
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: 'white',
        transform: [
            {scaleX: 2}
        ]
    },
    pp: {
        marginLeft: 40,
        marginTop: 20,
        flexDirection: 'row'
    },
    acc: {
        alignSelf: 'center',
        marginLeft: 20
    },
    text2: {
        fontSize: 30,
        marginLeft: 30,
        marginTop: 25,
        fontFamily: 'sans-serif-medium'
    },
    foto: {
        height: 120,
        width: 350,
        marginTop: 20,
        borderRadius: 30
    },
    text3: {
        fontWeight: 'bold',
        color: 'white',
        position: 'absolute',
        bottom: 5,
        right: 5,
        fontSize: 20,
        color: 'black',
        backgroundColor: 'white'
    }
})