import { StatusBar } from 'expo-status-bar';
import React from 'react'
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import About from './Tugas13/AboutScreen'
import LogIn from './Tugas13/LoginScreen'
import Register from './Tugas13/RegisterScreen'
import Skill from './Tugas14/SkillScreen'
import Add from './AddScreen'
import Project from './ProjectScreen'
import Bahasa from './Tugas14/BahasaPemrograman'
import Framework from './Tugas14/Framework'
import Teknologi from './Tugas14/Teknologi'

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()
const Drawer = createDrawerNavigator()

const TabsScreen = () => (
    <Tabs.Navigator>
      <Tabs.Screen name="Skill" component={Skill}/>
      <Tabs.Screen name="Add" component={Add}/>
      <Tabs.Screen name="Project" component={Project}/>
    </Tabs.Navigator>
)

const DrawerScreen = () => {
  return (
    <Drawer.Navigator>
        <Drawer.Screen name="About" component={About}/>
        <Drawer.Screen name="Home" component={TabsScreen}/>
    </Drawer.Navigator>
  )
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="LogIn" component={LogIn}/>
        <Stack.Screen name="Register" component={Register}/>
        <Stack.Screen name="DrawerScreen" component={DrawerScreen}/>
        <Stack.Screen name="Bahasa" component={Bahasa}/>
        <Stack.Screen name="Framework" component={Framework}/>
        <Stack.Screen name="Teknologi" component={Teknologi}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

{/*export default () => (
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen name="LogIn" component={LogIn}/>
      <AuthStack.Screen name="Register" component={Register}/>
      <AuthStack.Screen name="About" component={About}/>
    </AuthStack.Navigator>
  </NavigationContainer>
) */}

//export default () => (
    //<LogIn/>
    //<DrawerScreen/>
    //<Register/>
    //<Skill/>
//)

//T_T sorry banget masih bingung taro drawer didalam stack, n taro stack di dalem tabscreen itu gimanaa, ga bisa bolak baliknya T_T